const axios = require('axios');

// Function to update labels
async function updateLabels() {
    try {        // Make GET request to fetch issue details
        const response = await axios.get(process.env.CI_API_V4_URL + '/projects/' + process.env.CI_PROJECT_ID + '/issues', {
            headers: {
                'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN
            }
        });

        // Iterate over each Issue
        for (const issue of response.data) { 
        // Check if the label 'init::template' exists
            if (issue.labels.includes('init::template')) {
            // Make PUT request to update the label
            await axios.put(process.env.CI_API_V4_URL + '/projects/' + process.env.CI_PROJECT_ID + '/issues/' + issue.iid, {
            labels: ['template'], 
            remove_labels: ['init::template']
            }, {
                headers: {
                    'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN
                }
        });

                console.log('Updated labels for issue ' + issue.title);
            } else {
                console.log('No action required');
            }
        }  
    } catch (error) {
        console.error('Error:', error.message);
    }
}

// Call the function to update label
updateLabels();



